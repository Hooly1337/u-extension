export default {
  content: ["./src/**/*.{ts,tsx}"],
  theme: {
    colors: {
      black: "#0C1310",
      grey: "#C4CBCA",
      violet: "#2A1E5C",
      pink: "#EE4266",
      lightblue: "#3CBBB1",
    },
    fontFamily: {
      roboto: ["Roboto", "sans-serif"],
    },
    extend: {},
  },
  plugins: [],
};
