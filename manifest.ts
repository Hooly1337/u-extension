import packageJson from "./package.json";

/**
 * After changing, please reload the extension at `chrome://extensions`
 */
const manifest: chrome.runtime.ManifestV3 = {
  manifest_version: 3,
  name: packageJson.name,
  version: packageJson.version,
  description: packageJson.description,
  background: {
    service_worker: "src/background/index.js",
    type: "module",
  },
  icons: {
    "128": "icon-128.png",
  },
  action: {},
  content_scripts: [
    {
      matches: ["https://steamcommunity.com/market/listings/*/*"],
      js: ["src/steam-page/index.js"],
      // KEY for cache invalidation
      css: [
        /*"assets/css/contentStyle<KEY>.chunk.css"*/
      ],
    },
    {
      matches: ["https://buff.163.com/goods/*"],
      js: ["src/buff-page/index.js"],
      // KEY for cache invalidation
      css: [
        /*"assets/css/contentStyle<KEY>.chunk.css"*/
      ],
    },
  ],
  web_accessible_resources: [
    {
      resources: ["assets/js/*.js", "assets/css/*.css", "icon-128.png", "icon-34.png"],
      matches: ["*://*/*"],
    },
  ],
  permissions: ["tabs"],
};

export default manifest;
