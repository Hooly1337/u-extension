import type { BuffGames, Message } from "@src/other/types";

const headerSelector = ".detail-header .detail-summ .l_Right";
const itemNameSelector = ".detail-cont h1";

const CONNECTION_NAME = "buff-page";
const port = chrome.runtime.connect({ name: CONNECTION_NAME });

port.onMessage.addListener((msg: Message) => {
  if (msg.type != "steamPriceResponse") {
    return;
  }

  const priceBlock = document.querySelectorAll("tbody>.selling>td>div[style='display: table-cell;']");
  priceBlock.forEach((el) => {
    const buffPrice = parseFloat(el.querySelector("span")!.textContent!.match(/\(₽ (.*)\)/)![1]);
    const percent = document.createElement("span");
    const price = document.createElement("span");

    percent.setAttribute("style", "color: green; opacity: 0.8; display: block");
    percent.textContent = `${calcPercent(msg.content.minPrice, buffPrice)}%`;
    el.append(percent);

    price.textContent = `${msg.content.minPrice} р.`;
    price.setAttribute("style", "color: darkblue; display: block;");

    el.append(price);
  });
});

const getGame = (): BuffGames => {
  const [gameName] = document.body.classList;
  if (gameName) {
    switch (gameName.trim()) {
      case "csgo":
        return 730;
      case "dota2":
        return 530;
    }
  }
  throw new Error("unknown game");
};

const calcPercent = (steam: number, buff: number): string => {
  return (((steam * 0.87) / buff - 1) * 100).toFixed(2);
};

const handleClick = async (e) => {
  e.preventDefault();
  const itemName = document.querySelector(itemNameSelector).textContent.trim();
  const game = getGame();
  const data: Message = {
    type: "loadSteamPrice",
    content: { name: itemName, game },
  };
  await port.postMessage(data);
};

const addButton = () => {
  const buttonsBlock = document.querySelector(headerSelector);

  const button = document.createElement("a");

  button.style.background = "transparent";
  button.style.padding = "5px 6px";
  button.style.color = "white";
  button.style.cursor = "pointer";
  button.style.border = "1px solid white";
  button.style.borderRadius = "2px";

  button.textContent = "load steam";
  button.addEventListener("click", handleClick);

  buttonsBlock.prepend(button);
};

addButton();
