/// <reference types="chrome"/>
import type { BuffMessage, SteamMessage } from "@src/other/types";

const CONNECTIONS = {
  buffPage: "buff-page",
  steamPage: "steam-page",
} as const;

const PRICE_REGEX = new RegExp('<span class="market_listing_price market_listing_price_with_fee">(.*?)<', "gs");

const buffPorts = new Map<string, chrome.runtime.Port>();

chrome.action.onClicked.addListener(async () => {
  await chrome.windows.create({
    url: "src/app/index.html",
    type: "normal",
    width: 1200,
    height: 700,
  });
});

chrome.runtime.onConnect.addListener(async (port) => {
  if (port.name == CONNECTIONS.buffPage) {
    port.onMessage.addListener(async (msg: BuffMessage) => {
      switch (msg.type) {
        case "loadSteamPrice": {
          const minPrice = await fetchPrice(msg.content.name);

          const message: SteamMessage = {
            type: "steamPriceResponse",
            content: {
              minPrice,
              name: msg.content.name,
            },
          };

          port.postMessage(message);
          break;
        }
        case "addItemFromBuff": {
          // TODO: add item to fav
          break;
        }
      }
    });
  }
  if (port.name == CONNECTIONS.steamPage) {
    port.onMessage.addListener((msg: SteamMessage) => {
      switch (msg.type) {
        case "steamPriceResponse": {
          const buffPort = buffPorts.get(msg.content.name);

          buffPort.postMessage(msg);
          break;
        }
        case "addItemFromSteam": {
          // TODO: add item to fav
        }
      }
    });
  }
});

const fetchPrice = async (name: string): Promise<number> => {
  const res = await fetch(`https://steamcommunity.com/market/listings/730/${encodeURIComponent(name)}`, {
    method: "GET",
  });

  if (res.status === 200) {
    const prices: number[] = [];
    const body = await res.text();
    for (const price of body.matchAll(PRICE_REGEX)) {
      prices.push(parseFloat(price[1].trim().replace(",", ".")));
    }

    return Math.min(...prices);
  }
};
