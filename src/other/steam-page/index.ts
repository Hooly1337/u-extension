import { Message } from "@src/other/types";

const ITEM_ACTION_SELECTOR = "#largeiteminfo_item_actions";

const sleep = (time: number) => new Promise((resolve) => setTimeout(resolve, time * 1000));

const handleClick = () => {
  const itemName = location.href.split("/").at(-1).trim();

  const body: Message = {
    type: "addItem",
    content: {
      name: itemName,
    },
  };

  chrome.runtime.sendMessage(body).then();
};

const createButton = () => {
  const button = document.createElement("a");
  button.text = "Add to fav";
  button.classList.add("btn_small", "btn_grey_white_innerfade");
  button.addEventListener("click", handleClick);
  return button;
};
const init = async () => {
  let wrapper = document.querySelector(ITEM_ACTION_SELECTOR);
  let i = 0;
  while (!wrapper && i < 5) {
    await sleep(0.3);
    wrapper = document.querySelector(ITEM_ACTION_SELECTOR);
    i++;
  }

  wrapper.append(createButton());
};

init();
