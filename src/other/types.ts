export type Message =
  | {
      type: "addItem";
      content: {
        name: string;
      };
    }
  | {
      type: "loadSteamPrice";
      content: {
        name: string;
        game: BuffGames;
      };
    }
  | {
      type: "steamPriceResponse";
      content: {
        name: string;
        minPrice: number;
      };
    };

export type BuffGames = 730 | 530;
