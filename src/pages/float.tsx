import { Select } from "@shared/ui";
import { useState } from "react";

export const FloatTable = () => {
  const [active, setActive] = useState("1");

  return (
    <>
      <Select
        value={active}
        onChange={setActive}
        variants={["1", "2", "44444444444 44444 444444"]}
      />
    </>
  );
};
