import React, { useEffect, useRef, useState } from "react";

export type EditableCellProps = {
  value: string | number;
  onChange: (value: string) => void;
};
export const EditableCell = ({ value, onChange }: EditableCellProps) => {
  const [isEditable, setEditable] = useState<boolean>(false);
  const inputRef = useRef<HTMLInputElement>(null);

  const saveValue = (e: React.FocusEvent<HTMLInputElement>) => {
    onChange(e.target.value);
    setEditable(false);
  };

  const edit = () => {
    setEditable(true);
  };

  useEffect(() => {
    if (isEditable) {
      inputRef.current.focus();
    }
  }, [isEditable]);

  return (
    <div>
      {isEditable ? (
        <input
          type="text"
          ref={inputRef}
          defaultValue={value}
          onBlur={saveValue}
        />
      ) : (
        <button onClick={edit}>{value}</button>
      )}
    </div>
  );
};
