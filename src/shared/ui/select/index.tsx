import clsx from "clsx";
import { useRef, useState } from "react";

import { useClickOutside } from "@shared/hooks";

type SelectProps = {
  onChange: (value: string) => void;
  variants: string[];
  value: string;
};
export const Select = ({ onChange, variants, value }: SelectProps) => {
  const [isActive, setActive] = useState(false);
  const wrapper = useRef<HTMLDivElement>();

  useClickOutside(wrapper, () => setActive(false));

  const handleChange = (value: string) => {
    return () => {
      setActive(false);
      onChange(value);
    };
  };

  return (
    <div className={clsx("relative w-1/6")} ref={wrapper}>
      <button
        className={clsx(
          "w-full rounded-md border border-grey p-3 text-left",
          isActive && "rounded-b-none border-b-0",
        )}
        onClick={() => setActive(!isActive)}
      >
        {value}
      </button>
      {isActive && (
        <div className="absolute w-full rounded-md rounded-t-none border border-t-0 border-grey p-3">
          {variants.map((el) => {
            return (
              <button
                className="block w-full pb-1 pt-1 text-left text-lightblue hover:text-pink"
                key={el}
                onClick={handleChange(el)}
              >
                {el}
              </button>
            );
          })}
        </div>
      )}
    </div>
  );
};
