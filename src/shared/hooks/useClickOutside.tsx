import { useEffect } from "react";
import type { MutableRefObject } from "react";

export const useClickOutside = (
  ref: MutableRefObject<HTMLElement>,
  callback: () => void
) => {
  const handleClickOutside = (event) => {
    if (ref.current && !ref.current.contains(event.target)) {
      callback();
    }
  };

  useEffect(() => {
    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);
};
