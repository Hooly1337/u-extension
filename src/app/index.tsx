import React from "react";
import refreshOnUpdate from "virtual:reload-on-update-in-view";

import { FloatTable } from "@pages";

import { createRoot } from "react-dom/client";
import "tailwindcss/tailwind.css";

refreshOnUpdate("./");

const App = () => {
  return (
    <div className="min-h-screen bg-black p-3 font-roboto text-base text-grey">
      <FloatTable />
    </div>
  );
};

function init() {
  const appContainer = document.querySelector("#root");
  if (!appContainer) {
    throw new Error("Can not find #root");
  }
  const root = createRoot(appContainer);
  root.render(<App />);
}

init();
